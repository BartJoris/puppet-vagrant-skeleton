notice("I'm node ${::fqdn} with IP ${::ipaddress_eth0}")

notify { 'my_message':
  message => hiera('my_message'),
}

  $packages = [
  "tree",
  "vim",
  "htop",
  "zsh",
  "git",]

package { $packages:
           ensure   => present,
        }